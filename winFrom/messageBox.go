// Copyright 2013 The Walk Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package winFrom

import (
//"fmt"
//"strings"
)

import (
	"github.com/lxn/walk"
	. "github.com/lxn/walk/declarative"
)

func RunDialogOKInfoDownload(mw *MyMainWindow) (int, error) { //walk.Form
	var dlg *walk.Dialog
	//var db *walk.DataBinder
	//var ep walk.ErrorPresenter

	var acceptPB, cancelPB *walk.PushButton
	owner := walk.Form(mw)
	var str string

	str = "已经开始执行"
	var tb Tb_url
	tb.ExecUrlListSql(`update UrlCount set buttonint='1' `)

	return Dialog{
		AssignTo:      &dlg,
		Title:         "状态",
		DefaultButton: &acceptPB,
		CancelButton:  &cancelPB,

		MinSize: Size{200, 200},
		Layout:  VBox{},
		Children: []Widget{
			Composite{
				Layout: Grid{},
				Children: []Widget{
					Label{
						Row:    0,
						Column: 0,
						Text:   " " + str,
					},
				},
			},
			Composite{
				Layout: HBox{},
				Children: []Widget{
					HSpacer{},
					PushButton{
						AssignTo: &acceptPB,
						Text:     "确定",
						OnClicked: func() {

							dlg.Accept()

						},
					},
				},
			},
		},
	}.Run(owner)
}

func RunDialogOKCancel_URL_InfoDownload(mw *MyMainWindow) (int, error) { //walk.Form
	var dlg *walk.Dialog
	//var db *walk.DataBinder
	//var ep walk.ErrorPresenter

	var acceptPB, cancelPB *walk.PushButton
	owner := walk.Form(mw)
	var str string

	str = "是否结束执行"

	return Dialog{
		AssignTo:      &dlg,
		Title:         "状态",
		DefaultButton: &acceptPB,
		CancelButton:  &cancelPB,

		MinSize: Size{200, 200},
		Layout:  VBox{},
		Children: []Widget{
			Composite{
				Layout: Grid{},
				Children: []Widget{
					Label{
						Row:    0,
						Column: 0,
						Text:   " " + str,
					},
				},
			},
			Composite{
				Layout: HBox{},
				Children: []Widget{
					HSpacer{},
					PushButton{
						AssignTo: &acceptPB,
						Text:     "确定",
						OnClicked: func() {
							mw.ButtonInt = false
							var tb Tb_url
							tb.ExecUrlListSql(`update UrlCount set buttonint='0' `)
							dlg.Accept()

						},
					},
					PushButton{
						AssignTo:  &cancelPB,
						Text:      "取消",
						OnClicked: func() { dlg.Cancel() },
					},
				},
			},
		},
	}.Run(owner)
}

func RunDialogOKCancel_URL_RDownload(mw *MyMainWindow, str string) (int, error) { //walk.Form
	var dlg *walk.Dialog
	//var db *walk.DataBinder
	//var ep walk.ErrorPresenter

	var acceptPB, cancelPB *walk.PushButton
	owner := walk.Form(mw)

	return Dialog{
		AssignTo:      &dlg,
		Title:         "添加url",
		DefaultButton: &acceptPB,
		CancelButton:  &cancelPB,

		MinSize: Size{200, 200},
		Layout:  VBox{},
		Children: []Widget{
			Composite{
				Layout: Grid{},
				Children: []Widget{
					Label{
						Row:    0,
						Column: 0,
						Text:   "url重复" + str,
					},
				},
			},
			Composite{
				Layout: HBox{},
				Children: []Widget{
					HSpacer{},
					PushButton{
						AssignTo: &acceptPB,
						Text:     "重新下载",
						OnClicked: func() {

							var tb Tb_url
							tb.ExecUrlListSql(`update Tb_url set isDownload='-1' where  url= '` + str + `'`)
							mw.Urlmodel.ResetRows()

							dlg.Accept()
						},
					},
					PushButton{
						AssignTo:  &cancelPB,
						Text:      "取消",
						OnClicked: func() { dlg.Cancel() },
					},
				},
			},
		},
	}.Run(owner)
}
