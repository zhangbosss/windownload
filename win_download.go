package main

import (
	//"fmt"
	"io"
	"net/http"
	"os"
	"strings"
	//	"strconv"
)

const (
	UA = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:26.0) Gecko/20100101 Firefox/26.0"
)

func httpDownload1(url string) {

	var strSqlit []string
	strSqlit = strings.Split(url, "/")
	f, err := os.OpenFile("./"+strSqlit[strings.Count(url, "/")], os.O_APPEND|os.O_RDWR|os.O_CREATE, 0777) //其实这里的 O_RDWR应该是 O_RDWR|O_CREATE，也就是文件不存在的情况下就建一个空文件，但是因为windows下还有BUG，如果使用这个O_CREATE，就会直接清空文件，所以这里就不用了这个标志，你自己事先建立好文件。
	if err != nil {
		panic(err)
	}
	var req http.Request
	req.Method = "GET"
	req.Close = true
	req.URL, err = req.URL.Parse(url)
	if err != nil {
		panic(err)
	}
	header := http.Header{}

	header.Add("Accept-Encoding", "gzip,deflate,sdch")
	header.Add("Connection", "keep-alive")

	header.Add("Accept-Language", "zh-cn,zh;q=0.8,en-us;q=0.5,en;q=0.3")
	header.Add("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
	header.Add("User-Agent", UA)
	//header.Add("Range", "bytes="+string(stat.Size())+"-") 续传

	req.Header = header
	resp, err := http.DefaultClient.Do(&req)
	if err != nil {
		panic(err)
	}

	written, err := io.Copy(f, resp.Body)
	if err != nil {
		panic(err)
	}
	println("written: ", written)

}

/*func main() {
	mw := new(MyMainWindow)

	mw.Urlmodel = NewUrlModel()
	mw.Urlmodel.Num1 = strconv.Itoa(mw.Urlmodel.RetUrlDownlodingCount())
	mw.Urlmodel.Ini_Tb_url(`select top ` + mw.Urlmodel.Num1 + `  urlid,url,isDownload,startTime,endTime
		from Tb_url where isDownload='2' `)
	Formshow(mw)

}*/
